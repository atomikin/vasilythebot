import json

from app.data.message import Message
from app.data.update import Update
from app.data.user import User
from app.tests.fixtures.fresh import *


def test_update_creation(single_update: Update) -> None:
    u = single_update
    assert isinstance(u.dumps(), str)
    assert isinstance(repr(u), str)


def test_user_creation(user_only_required: User):
    data = {'id': 1, 'first_name': 'Петя', 'last_name': 'Петров', 'username': 'нагибатор666'}
    user = User(**data)
    print(user.dumps())
    print(user_only_required.dumps())
    assert user.id == 1
    assert user.first_name == 'Петя'
    assert user.last_name == 'Петров'
    assert user.username == 'нагибатор666'


def test_message_entity_creation_serialization():
    data = {
        'from': {'first_name': 'Aleksey', 'last_name': 'Abramov', 'username': 'atomikin', 'id': 145465209},
        'text': '4343',
        'message_id': 20,
        'date': 1485097190,
        'chat': {'all_members_are_administrators': False, 'type': 'group', 'title': 'Дом', 'id': -169494404},
        'entities': [{'type': 'bot_command', 'length': 7, 'offset': 5}]
    }
    assert False
    message = Message(**data)
    print(message)

    print(message.dumps())


