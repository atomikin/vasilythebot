import pytest

from app.data.update import Update
from app.data.user import User


@pytest.fixture()
def single_update():
    data = {
        'from': {'first_name': 'Aleksey', 'last_name': 'Abramov', 'username': 'atomikin', 'id': 145465209},
        'text': '4343',
        'message_id': 20,
        'date': 1485097190,
        'chat': {'all_members_are_administrators': False, 'type': 'group', 'title': 'Дом', 'id': -169494404},
        'entities': [{'type': 'bot_command', 'length': 7, 'offset': 5}]
    }
    return Update(**data)


@pytest.fixture()
def user_only_required():
    return User(id=1, first_name='Петя')
