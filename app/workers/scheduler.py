import asyncio
import logging
from asyncio import Queue
from datetime import timedelta, datetime

from app.common.abstracts import AbstractWorker, WorkerState, Config
from app.common.scheduled_task import ScheduledTask


class Scheduler(AbstractWorker):
    pool = []
    queue = Queue()
    tasks = list()
    # index to support fast lookup by (type, chat_id) if task need to avoid duplicates
    INDEX = dict()

    async def process(self, task: ScheduledTask):
        task.last_run = datetime.now()
        await task.run()

    async def run(self):
        logger = logging.getLogger('console')
        self.state = WorkerState.RUNNING
        while self.state is not WorkerState.STOPPED:
            while not self.queue.empty():
                try:
                    task = self.queue.get_nowait()
                    logger.debug('Processing: {}'.format(task))
                    if (task.NAME, task.chat_id) not in self.INDEX:
                        self.tasks.append(task)
                        self.INDEX[(task.NAME, task.chat_id)] = task
                    else:
                        logger.debug('Ignoring duplicate task, {}'.format(task))
                except asyncio.QueueEmpty:
                    pass
            logger.debug('Task queue: {}'.format(self.tasks))
            for i, task in enumerate(sorted(self.tasks, key=lambda t: t.last_run)):
                if task.invalid_after <= datetime.now():
                    self.tasks.pop(i)
                    self.INDEX.pop((task.NAME, task.chat_id))
                    continue
                if task.last_run and task.last_run + task.period > datetime.now():
                    break
                elif task.last_run + task.period <= datetime.now():
                    await self.process(task)
            await asyncio.sleep(Config.DATA['scheduler_period'])
