import logging
from asyncio import Queue

from app.common.abstracts import AbstractWorker
from app.data.message import MessageEntity
from app.data.update import Update
from app.handlers import CommandIndex


class UpdateProcessor(AbstractWorker):

    pool = []
    queue = Queue()

    async def process(self, update: Update) -> None:
        logger = logging.getLogger('console')
        if update.message:
            handler = self.dispatch(update)
            logger.debug(repr(handler))
            if handler:
                await handler(
                    text=update.message.text,
                    chat_id=update.message.chat.id,
                    reply_to_id=update.message.message_id
                ).run()

    @staticmethod
    def dispatch(update: Update):
        entity = update.message.entities[0] if update.message.entities else None
        handler = None
        if entity and entity.type is MessageEntity.Type.BOT_COMMAND:
            command = update.message.text[entity.offset or None:entity.length].strip('/')
            try:
                handler = CommandIndex.INDEX.get(command)
            except AttributeError:
                handler = None
        return handler







