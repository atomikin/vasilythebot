import asyncio
import logging
from asyncio import Queue
from http import HTTPStatus

from aiohttp import ClientSession

from app.common.abstracts import AbstractWorker, Config
from app.data.json_serializable import AbstractModel


class MessageSender(AbstractWorker):
    pool = []
    queue = Queue()
    MIME_TYPE_HEADER = {'content-type': 'application/json'}

    async def process(self, message: AbstractModel):
        logger = logging.getLogger('console')
        logger.debug('processed by: {}'.format(self.id))
        async with ClientSession() as session:
            async with session.post(
                    Config.DATA.send_message_url, data=message.dumps(), headers=self.MIME_TYPE_HEADER
            ) as response:
                # TODO: switch to enum
                if response.status == 200:
                    logger.info(('Successfully send message. Response: ' + repr(await response.json())))
                else:
                    logger.error(
                        'Error occurred: {}, {}'.format(await response.text(), response.status)
                    )
