import asyncio
import logging
from typing import List

from aiohttp import ClientSession

from app.common.abstracts import AbstractWorker, WorkerState, Config
from app.data.update import Update
from app.workers.update_processor import UpdateProcessor


class UpdateFetcher(AbstractWorker):

    pool = list()

    def __init__(self):
        super().__init__()
        self.offset = 0

    async def process(self, tasks: List[Update]):
        for task in tasks:
            await UpdateProcessor().queue.put(task)

    async def run(self):
        logger = logging.getLogger('console')
        self.state = WorkerState.RUNNING
        while self.state is not WorkerState.STOPPED:
            async with ClientSession() as session:
                params = {'timeout': Config.DATA['long_polling_timeout_secs'], 'offset': self.offset}
                try:
                    async with session.get(Config.DATA.updates_url, params=params) as response:
                        res_json = await response.json(encoding='utf-8')
                        if res_json['result']:
                            updates = [Update(**u) for u in res_json['result']]
                            self.offset = max(updates, key=lambda updt: updt.update_id).update_id + 1
                            await self.process(updates)
                except asyncio.TimeoutError as err:
                    logger.error(err.message)
