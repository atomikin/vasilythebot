from app.data.json_serializable import AbstractModel


class User(AbstractModel):

    def __init__(self, *, id: int, first_name: str, **kwargs):
        self.id = id
        self.first_name = first_name
        self.last_name = kwargs.get('last_name')
        self.username = kwargs.get('username')

    def __hash__(self):
        h = 17
        h += self.id
        h += hash(self.last_name)
        h += hash(self.first_name)
        h += hash(self.username)
        return h