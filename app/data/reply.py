from app.data.json_serializable import AbstractModel
from app.data.helpers import get


class Reply(AbstractModel):

    def __init__(self, *, chat_id: int, text: str, **kwargs):
        self.text = text
        self.chat_id = chat_id
        self.parse_mode = kwargs.get('parse_mode')
        self.disable_web_page_preview = kwargs.get('disable_web_page_preview')
        self.disable_notification = kwargs.get('disable_notification')
        self.reply_to_message_id = kwargs.get('reply_to_message_id')
        self.reply_markup = kwargs.get('reply_markup')
