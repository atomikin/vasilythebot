from app.data.json_serializable import AbstractModel


class Chat(AbstractModel):

    def __init__(self, *, type, id, **kwargs):
        self.id = id
        self.type = type
        self.title = kwargs.get('title')
        self.username = kwargs.get('username')
        self.first_name = kwargs.get('first_name')
        self.last_name = kwargs.get('last_name')
        self.all_members_are_administrators = kwargs.get('all_members_are_administrators')

    def __hash__(self):
        h = 17
        h += self.id
        h += hash(self.type)
        h += hash(self.title)
        h += hash(self.username)
        h += hash(self.first_name)
        h += hash(self.last_name)
        h += hash(self.all_members_are_administrators)
        return h
