import json
from abc import ABCMeta, abstractmethod
from datetime import datetime
from enum import Enum
from typing import Dict, T


class Serializable(metaclass=ABCMeta):

    @abstractmethod
    def dumps(self) -> str:
        pass

    @classmethod
    @abstractmethod
    def loads(cls, data: str) -> Dict[str, T]:
        pass


class JsonSerializable(Serializable):

    @staticmethod
    def __process_value(v):
        if isinstance(v, JsonSerializable):
            v = {i: j for i, j in v.__dict().items() if j}
        elif isinstance(v, datetime):
            v = str(v)
        elif isinstance(v, Enum):
            v = v.value
        return v

    def __dict(self) -> dict:
        res = dict()
        for k, v in self.__dict__.items():
            if not v:
                continue
            if isinstance(v, list):
                v = [self.__process_value(i) for i in v if i is not None]
            else:
                v = self.__process_value(v)
            res[k] = v
        return res

    def dumps(self):
        return json.dumps(self.__dict())

    @classmethod
    def loads(cls, data: str) -> Serializable:
        return cls(**json.loads(data))

    def __repr__(self):
        return '<{} {}>'.format(
            type(self).__name__,
            ' '.join(['{}={}'.format(k, v) for k, v in self.__dict__.items()])
        )


class AbstractModel(JsonSerializable):
    pass
