from app.data.json_serializable import AbstractModel


class Audio(AbstractModel):

    def __init__(self, *, file_id: str, duration: int, **kwargs):

        self.file_id = file_id
        self.duration = duration
        self.performer = kwargs.get('performer')
        self.title = kwargs.get('title')
        self.mime_type = kwargs.get('mime_type')
        self.file_size = kwargs.get('file_size')
