from typing import Dict, T

from app.data.json_serializable import AbstractModel
from datetime import datetime

from app.data.chat import Chat
from app.data.message_entity import MessageEntity
from app.data.user import User


class Message(AbstractModel):

    def __init__(self, *, message_id: int, chat: Dict[str, T], date: int, **kwargs):
        self.message_id = message_id
        self.date = datetime.utcfromtimestamp(date)
        self.chat = Chat(**chat)
        # optionals
        self.from_user = User(**kwargs.get('from_user')) if 'user' in kwargs else None
        self.forward_from = User(**kwargs.get('from_user')) if 'forward_from' in kwargs else None
        self.forward_from_chat = Chat(**kwargs.get('forward_from_chat')) if 'forward_from_chat' in kwargs else None
        self.forward_from_message_id = kwargs.get('forward_from_message_id')
        self.forward_date = kwargs.get('forward_date')
        self.reply_to_message = kwargs.get('reply_to_message')
        self.edit_date = datetime.utcfromtimestamp(kwargs.get('edit_date')) if 'edit_date' in kwargs else None
        self.text = kwargs.get('text')
        self.entities = [MessageEntity(**e) for e in kwargs['entities']] if 'entities' in kwargs else None

    def __hash__(self):
        h = 17
        h += self.message_id
        h += hash(self.date)
        h += hash(self.chat)
        h += hash(self.from_user)
        h += hash(self.forward_from)
        h += hash(self.forward_from_chat)
        h += self.forward_from_message_id or 0
        h += hash(self.forward_date)
        h += self.reply_to_message or 0
        h += hash(self.edit_date)
        h += hash(self.text)
        h += sum([hash(e) for e in self.entities if self.entities] or [0])
        return abs(h)



