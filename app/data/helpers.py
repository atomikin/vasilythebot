from app.data.json_serializable import AbstractModel
from typing import Callable


def get(d: dict, key: str, cls: Callable[[dict], AbstractModel]) -> AbstractModel:
    return cls(**d[key]) if key in d else None
