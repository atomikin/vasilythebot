from enum import Enum

from app.data.json_serializable import AbstractModel
from app.data.user import User


class MessageEntity(AbstractModel):

    class Type(Enum):

        MENTION = 'mention'
        HASHTAG = 'hashtag'
        BOT_COMMAND = 'bot_command'
        EMAIL = 'email'
        URL = 'url'
        BOLD = 'bold'
        ITALIC = 'italic'
        CODE = 'code'
        PRE = 'pre'
        TEXT_LINK = 'text_link'
        TEXT_MENTIONS = 'text_mentions'

    def __init__(self, *, type: str, offset: int, length: int, **kwargs):
        self.type = MessageEntity.Type(type)
        self.offset = offset
        self.length = length
        self.url = kwargs.get('url')
        self.user = User(**kwargs.get('user')) if 'user' in kwargs else None

    def __hash__(self):
        h = 17
        h += hash(self.type.value)
        h += self.offset
        h += self.length
        h += hash(self.url)
        h += hash(self.user)
        return abs(h)
