from app.data.json_serializable import AbstractModel
from app.data.message import Message
from app.data.helpers import get


class Update(AbstractModel):

    def __init__(self, *, update_id, **kwargs):
        self.update_id = update_id
        self.message = get(kwargs, 'message', Message)
        self.edited_message = get(kwargs, 'edited_message', Message)
        self.chanel_post = get(kwargs, 'chanel_post', Message)
        self.edited_chanel_post = get(kwargs, 'edited_chanel_post', Message)
        self.inline_query = kwargs.get('inline_query')
        self.chosen_inline_result = kwargs.get('chosen_inline_result')
        self.callback_query = kwargs.get('callback_query')

    def __hash__(self):
        h = 17
        # h += self.update_id
        h += hash(self.message)
        h += hash(self.edited_message)
        h += hash(self.chanel_post)
        h += hash(self.edited_chanel_post)
        # TBD. enough for now

        return abs(h)
