import asyncio
from abc import ABCMeta, abstractmethod
from collections import namedtuple
from copy import deepcopy
from enum import IntEnum, Enum

import yaml


class WorkerState(IntEnum):
    IDLE = 0
    RUNNING = 1
    SLEEPING = 2
    STOPPED = 3


class Config(Enum):

    DATA = 'data'

    def __init__(self, dummy):
        with open('./app/config/data.yml', 'r') as f:
            self.__data = yaml.load(f)

    def __getitem__(self, item):
        return deepcopy(self.__data[item])

    @property
    def authorized_url(self):
        return '{}/bot{}'.format(self['base_url'], self['token'])

    @property
    def updates_url(self):
        return self.authorized_url + self['endpoints']['get_updates']

    @property
    def send_message_url(self):
        return self.authorized_url + self['endpoints']['send_message']


class Runnable(metaclass=ABCMeta):

    @abstractmethod
    async def run(self) -> None:
        pass


class AbstractWorker(Runnable):
    pool = []
    queue = None
    id = 0

    def __init__(self):
        self.state = WorkerState.IDLE
        self.pool.append(self)
        self.id += 1
        type(self).id += 1

    @abstractmethod
    async def process(self, task):
        pass

    async def run(self) -> None:
        self.state = WorkerState.RUNNING
        while self.state is not WorkerState.STOPPED:
            await self.process(await self.queue.get())




















