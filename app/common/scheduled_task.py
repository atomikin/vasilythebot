from abc import abstractmethod
from datetime import datetime

from app.common.abstracts import Runnable


class ScheduledTask(Runnable):

    NAME = None

    def __init__(
            self, *, chat_id: int, instruction: str,
            invalid_after: datetime, period: int, reply_to_id: int):
        self.invalid_after = invalid_after
        self.period = period
        self.last_run = datetime.now()
        self.chat_id = chat_id
        self.instruction = instruction
        self.reply_to_id = reply_to_id

    @abstractmethod
    async def run(self):
        pass

    def __repr__(self):
        return '<{} chat_id={} name={}>'.format(type(self).__name__, repr(self.chat_id), repr(self.NAME))
