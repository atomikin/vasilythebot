

class CommandIndex(type):
    INDEX = {}

    def __new__(mcs, name, bases, class_dict):
        cls = type.__new__(mcs, name, bases, class_dict)
        mcs.INDEX[class_dict.get('NAME') or name] = cls
        return cls
