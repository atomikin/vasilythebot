from collections import namedtuple

from app.handlers.command_index import CommandIndex


class AbstractCommand(metaclass=CommandIndex):

    NAME = None
    INDEX = {}

    ParsedCommand = namedtuple('ParsedCommand', ['name', 'args'])

    async def run(self)-> None:
        raise NotImplementedError('run is not implemented')

    def __init__(self, *, text: str, chat_id: int, reply_to_id: int):
        args = text.split(' ')
        if len(args) == 2:
            self.instruction = self.ParsedCommand(args[1], tuple())
        elif len(args) > 2:
            self.instruction = self.ParsedCommand(args[1], tuple(args[2:]))
        else:
            self.instruction = None
        self.chat_id = chat_id
        self.reply_to = reply_to_id
