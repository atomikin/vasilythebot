import logging
from datetime import datetime, timedelta

from app.handlers.abstract_command import AbstractCommand
from app.common.scheduled_task import ScheduledTask
from app.data.reply import Reply
from app.workers.message_sender import MessageSender
from app.workers.scheduler import Scheduler

logger = logging.getLogger('console')


class Pomodoro(AbstractCommand):

    """
    It is not an instance of the Runnable as it would lead to a conflict of metaclasses
    """

    NAME = 'pomodoro'
    SESSION_EXISTS_MSG = (
        'У вас уже есть запущенная сессия Pomodoro.\n'
        'Чтобы начать новую тукущую сессию нужно отменить: \n'
        '`/pomodoro stop`'
    )

    WILL_STOP_MSG = 'Сеанс Pomodoro окончен. Спасибо!'
    STARTED_MSG_TEMPLATE = 'Сеанс Pomodoro начат. Впереди {} минут работы!'
    WRONG_REQUEST_MSG = 'Неверный запрос. Пример верного запроса\n\t /pomodoro start 30 5'
    INCORRECT_INSTRUCTION_MSG_TEMPLATE = (
        'Переданный тип команды ({}) пока не поддерживается.'
        'Возможно вы имели ввиду start, stop, help?'
    )

    HELP_MSG = 'Список команд:\n\t' \
               'start <work time> <rest time> - начать новую 8ми часовую сессию\n\t' \
               'stop - остановить сессию\n\t' \
               'help - получить данное сообщение'

    NO_SESSION_MSG = 'У вас нет запущенной сессии Pomodoro'
    INFO_MSG_TEMPLATE = 'Осталось {} минут {} секунд'

    STOP = 'stop'
    START = 'start'
    HELP = 'help'

    def __init__(self, *, text: str, chat_id: int, reply_to_id: int):
        super().__init__(text=text, chat_id=chat_id, reply_to_id=reply_to_id)

    async def process_start(self, task_id: tuple):
        """
        Process request to start a new session
        :param task_id: id of the task in Scheduler.INDEX
        :return: Returns text of the message. Creates tasks
        """
        logger.debug('Starting {}'.format(task_id))
        if task_id in Scheduler.INDEX:
            return self.SESSION_EXISTS_MSG
        try:
            int_args = [int(i) for i in self.instruction.args]
        except ValueError:
            int_args = []
        if not self.instruction or len(self.instruction.args) < 2 or len(int_args) < 2:
            text = self.WRONG_REQUEST_MSG
        else:
            text = self.STARTED_MSG_TEMPLATE.format(self.instruction.args[0])
            await Scheduler.queue.put(
                PomodoroTask(
                    chat_id=self.chat_id, instruction=self.instruction,
                    invalid_after=datetime.now() + timedelta(hours=8),
                    period=timedelta(minutes=int(self.instruction.args[0])), reply_to_id=self.reply_to
                )
            )
        return text

    async def process_help(self, task_id: int) -> str:
        """
        Process request to return help info
        :param task_id: id of the task in Scheduler.INDEX
        :return: text of the message.
        """
        logger.debug('Providing help by {}'.format(task_id))
        return self.HELP_MSG

    async def process_stop(self, task_id: int) -> str:
        """
        Stops the existent task or notifies that there is not such a task
        :param task_id: id of the task in Scheduler.INDEX
        :return:
        """
        logger.debug('Was requested to stop session for {}'.format(task_id))
        if task_id in Scheduler.INDEX:
            Scheduler.INDEX[task_id].invalid_after = datetime.now()
            return self.WILL_STOP_MSG
        return self.NO_SESSION_MSG

    async def process_info(self, task_id: int) -> str:
        """
        Returns the time left for the current activity
        :param task_id: id of the task in Scheduler.INDEX
        :return: text of the message
        """
        logger.debug('Was requested to provide info about {}'.format(task_id))
        if task_id not in Scheduler.INDEX:
            return self.NO_SESSION_MSG
        task = Scheduler.INDEX[task_id]
        next_run = task.period + task.last_run
        if next_run <= task.invalid_after:
            delta = next_run - datetime.now()
            return self.INFO_MSG_TEMPLATE.format(delta.seconds//60, delta.seconds % 60)
        return self.NO_SESSION_MSG

    async def run(self) -> None:
        """
        Mimics the runnable. Coroutine to be run by an UpdateProcessor
        :return: None
        """

        instructions = {
            'start': self.process_start,
            'help': self.process_help,
            'stop': self.process_stop,
            'info': self.process_info,
        }

        if not self.instruction:
            return
        task_id = (self.NAME, self.chat_id)
        if self.instruction.name in instructions:
            text = await instructions[self.instruction.name](task_id)
        else:
            text = self.INCORRECT_INSTRUCTION_MSG_TEMPLATE.format(self.instruction.name)
        logger.debug('Sending message: {}'.format(text))
        await MessageSender.queue.put(
            Reply(chat_id=self.chat_id, text=text)
        )


class PomodoroTask(ScheduledTask):
    NAME = 'pomodoro'
    BIG_BREAK_COUNT = 4

    WORK_IN_PROGRESS = 0
    REST_IN_PROGRESS = 1

    REST_MSG_TEMPLATE = 'Время отдохнуть. У вас {} минут.'
    WORK_MSG_TEMPLATE = 'Время поработать. Всего-то {} минут!'

    def __init__(
            self, *, chat_id: int, instruction: str,
            invalid_after: datetime, period: int, reply_to_id: int):
        super().__init__(chat_id=chat_id, instruction=instruction,
                         invalid_after=invalid_after, period=period, reply_to_id=reply_to_id)
        self.rest_count = 0
        self.status = self.WORK_IN_PROGRESS

    async def run(self) -> None:
        if self.status == self.WORK_IN_PROGRESS:
            self.status = self.REST_IN_PROGRESS
            self.period = timedelta(minutes=(
                15 if self.rest_count > 0 and
                not self.rest_count % self.BIG_BREAK_COUNT else int(self.instruction.args[1])
            ))
            self.rest_count += 1
            await MessageSender.queue.put(
                Reply(text=self.REST_MSG_TEMPLATE.format(self.period.seconds//60), chat_id=self.chat_id)
            )
        else:
            self.status = self.WORK_IN_PROGRESS
            self.period = timedelta(minutes=int(self.instruction.args[0]))
            await MessageSender.queue.put(
                Reply(text=self.WORK_MSG_TEMPLATE.format(self.period.seconds//60), chat_id=self.chat_id)
            )

