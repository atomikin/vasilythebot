"""
OBSOLETE!!!
"""

from random import randrange

from app.data.json_serializable import AbstractModel
from app.data.reply import Reply
from app.data.update import Update
from app import handlers


def hello(text: str, chat_id: int, reply_to_id: int) -> AbstractModel:
    """
    Эхо тест. Бот приветствует Вас =)
    """
    r = Reply(text='Всем чмоки в этом чате', reply_to_message_id=reply_to_id, chat_id=chat_id)
    # print(r.dumps())
    return r


def toss(text: str, chat_id: int, reply_to_id: int) -> AbstractModel:
    def toss_coin(expected):
        expected = 'орел' == expected.replace('ё', 'e') and 1 or 0
        res = randrange(2)
        printable_res = res and '(O) - орёл' or '(P) - решка'
        if res == expected:
            return 'Не угадали: ' + printable_res
        return 'Ура! Вы угадали: ' + printable_res

    def toss_dices(expected):
        try:
            expected = int(expected)
        except ValueError:
            return None
        res = [randrange(6) + 1 for _ in range(2)]
        sl = sum([expected == i for i in res])
        print('sl: ', sl, res)

        printable_res = '[{}] - [{}]'.format(*res)
        if sl == 1:
            return 'Успех! Вы угадали одно из чисел: ' + printable_res
        elif sl == 2:
            return 'Успех! Вы угадали оба числа!!: ' + printable_res
        return 'Вы набрали {} очков: '.format(sum(res)) + printable_res

    try:
        command, what, target = text.split(' ', 2)
    except ValueError:
        return Reply(text='{} - чего-то не хватает'.format(text), reply_to_message_id=reply_to_id, chat_id=chat_id)
    res = {
        'coin': toss_coin,
        'dices': toss_dices
    }.get(what.strip())
    if res:
        res = res(target.strip().lower())
        print(res)
        return Reply(text=res, reply_to_message_id=reply_to_id, chat_id=chat_id)


def pomodoro(text: str, chat_id: int, reply_to_id: int) -> AbstractModel:

    async def work_start():

        return Reply(text=''

        )

    _, action, text.split(' ')




















