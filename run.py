import argparse
import asyncio
import itertools
import logging
import logging.config

from pathlib import Path

import yaml
from app.workers.message_sender import MessageSender
from app.workers.scheduler import Scheduler
from app.workers.update_fetcher import UpdateFetcher
from app.workers.update_processor import UpdateProcessor


with Path(Path(__file__).parent).joinpath('app', 'config', 'logging.yml').open() as f:
    logging.config.dictConfig(yaml.load(f))


def main():
    logger = logging.getLogger('console')

    parser = argparse.ArgumentParser()
    parser.add_argument(
        '--update-processors', help='Number of update processors', default=4, type=int
    )
    parser.add_argument(
        '--message-senders', help='Number of message senders', default=4, type=int
    )
    parser.add_argument('--schedulers', help='Number of schedulers', default=1,  type=int)
    parser.add_argument('--debug', action='store_true', help='Debug mode')
    parsed = parser.parse_args()
    logger.info(
        'schedulers: {}, message senders: {}, update processors: {}'
        .format(parsed.schedulers, parsed.message_senders, parsed.update_processors)
    )
    if parsed.debug:
        logger.setLevel('DEBUG')
    logger.info('VasilyTheBot - started')
    for _ in range(parsed.update_processors):
        UpdateProcessor()
    for _ in range(parsed.message_senders):
        MessageSender()
    for _ in range(parsed.schedulers):
        Scheduler()

    loop = asyncio.get_event_loop()
    futures = asyncio.gather(
        UpdateFetcher().run(),
        *(i.run() for i in itertools.chain(Scheduler.pool, UpdateProcessor.pool, MessageSender.pool))
    )
    try:
        loop.run_until_complete(futures)
    except Exception as err:
        # shutdown gracefully
        logger.info('Shutting down. Reason: {}'.format(str(err)))
    else:
        loop.close()


if __name__ == '__main__':
    main()
